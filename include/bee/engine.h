#ifndef BEE_ENGINE_H
#define BEE_ENGINE_H

#include "bee/scene.h"

/**
 * Initializes and runs engine. Register all hooks before calling this function.
 * @return
 */
int beeEngine_Initialize(int, char **);

/**
 * Denitializes and stops engine
 * @return
 */
void beeEngine_Deinitialize(void);

/**
 *
 * @returns count of frames per second as float value. To get integer count - cast to int
 */
float beeEngine_GetFPS(void);

/**
 *
 * @returns time past from the last frame in seconds
 */
float beeEngine_GetDeltaTime(void);

/**
 * Registers new scene in engine
 * @param scene - scene to register
 * @returns if the same <b>scene</b> is already registered, result is <b>-1</b>,<br>
 * otherwise result is 0
 */
int beeEngine_RegisterScene(beeScene *scene);

/**
 * Unregisters scene in engine
 * @param scene - scene to unregister
 * if scene is not in the registered list - result is <b>noop</b>
 */
void beeEngine_UnregisterScene(beeScene *scene);

#endif  // BEE_ENGINE_H
