#ifndef BEE_VECTOR4_H
#define BEE_VECTOR4_H

#include "bee/beetypes.h"

typedef struct {
  BEE_FLOAT x, y, z, w;
} beeVec4;

/**
 *
 * @return new zero-initialized vector
 */
beeVec4 *beeMath_NewVec4(void);

void beeMath_FreeVec4(beeVec4* vector);

/**
 * Copies all components of the <b>source</b> vector to the <b>target</b>
 * @param target - target vector to copy values to
 * @param source - other vector to copy values from
 */
void beeMath_Vec4Copy(beeVec4* target, beeVec4* source);

BEE_FLOAT beeMath_Vec4MagnitudeSqr(beeVec4* vec);
BEE_FLOAT beeMath_Vec4Magnitude(beeVec4* vec);

void beeMath_Vec4Normalize(beeVec4* vec);
beeVec4 *beeMath_Vec4Normalized(beeVec4* vec);

BEE_FLOAT beeMath_Vec4DotVec4(beeVec4* vec1, beeVec4* vec2);
beeVec4 *beeMath_Vec4CrossVec4(beeVec4* vec1, beeVec4* vec2);

#endif //BEE_VECTOR4_H
