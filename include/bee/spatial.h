#ifndef BEE_SPATIAL_H
#define BEE_SPATIAL_H

#include "bee/mathf.h"
#include "bee/matrix4.h"
#include "vector4.h"

typedef struct {
  beeVec4 position;
  beeVec4 rotation;
  beeVec4 scale;
} beeSpatial;

beeSpatial *beeSpatial_NewSpatial(void);
void beeSpatial_FreeSpatial(beeSpatial *spatial);

void beeSpatial_SetPosition(beeSpatial *spatial, BEE_FLOAT px, BEE_FLOAT py, BEE_FLOAT pz);
void beeSpatial_SetPositionRelative(beeSpatial *spatial, BEE_FLOAT px, BEE_FLOAT py, BEE_FLOAT pz);
void beeSpatial_SetScale(beeSpatial *spatial, BEE_FLOAT sx, BEE_FLOAT sy, BEE_FLOAT sz);
void beeSpatial_SetRotationEuler(beeSpatial *spatial, BEE_FLOAT rx, BEE_FLOAT ry, BEE_FLOAT rz);

beeMat4 *beeSpatial_GetModelMatrix(beeSpatial *spatial);

#endif //BEE_SPATIAL_H
