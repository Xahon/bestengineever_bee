#ifndef BEE_SCRIPTING_H
#define BEE_SCRIPTING_H

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>


void beeScripting_InitScripting(void);
void beeScripting_DeinitScripting(void);

void beeScripting_CallLUAFunction(const char* name, int inputs, int outputs);

#endif //BEE_SCRIPTING_H
