#ifndef BEE_SCENE_H
#define BEE_SCENE_H

#include <lua.h>
#include "bee/spatial.h"
#include "bee/camera.h"

typedef struct {
  unsigned int id;
  beeSpatial *spatial;
  const char *name;
  beeCamera *camera;
} beeScene;

beeScene *beeObjects_NewScene(const char *);
void beeObjects_FreeScene(beeScene *scene);
void beeObjects_SetSceneCamera(beeScene *scene, beeCamera *camera);
void beeObjects_RenderScene(beeScene* scene);

/// SCRIPTING ///

int beeScriptingBinding_CreateScene(lua_State *L);


#endif //BEE_SCENE_H
