#ifndef BEE_CAMERA_H
#define BEE_CAMERA_H

#include <lua.h>
#include "bee/spatial.h"

typedef struct {
  unsigned int id;
  beeSpatial *spatial;
  BEE_FLOAT far;
  BEE_FLOAT near;
  BEE_FLOAT fov;
  BEE_FLOAT aspect;
} beeCamera;

beeCamera *beeObjects_NewCamera(BEE_FLOAT near, BEE_FLOAT far, BEE_FLOAT fov, BEE_FLOAT aspect);
void beeObjects_FreeCamera(beeCamera* camera);

beeMat4 *beeObjects_GetProjectionMatrix(beeCamera* camera);


/// SCRIPTING ///

int beeScriptingBinding_CreateCamera(lua_State *L);


#endif //BEE_CAMERA_H
