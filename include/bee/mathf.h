#ifndef BEE_MATHF_H
#define BEE_MATHF_H

#include <stdbool.h>
#include "bee/beetypes.h"

const BEE_FLOAT beeMath_PI;
const BEE_FLOAT beeMath_Rad2Deg;
const BEE_FLOAT beeMath_Deg2Rad;

bool beeMath_FloatEqPrec(BEE_FLOAT f1, BEE_FLOAT f2, BEE_FLOAT prec);
bool beeMath_FloatEqPrecDef(BEE_FLOAT f1, BEE_FLOAT f2);

#endif  // BEE_MATHF_H