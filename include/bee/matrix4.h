#ifndef BEE_MATRIX4_H
#define BEE_MATRIX4_H

#include <stdbool.h>
#include "bee/beetypes.h"

typedef struct {
  BEE_FLOAT m[16];
} beeMat4;

/**
 *
 * @return identity 4x4 matrix
 */
beeMat4 *beeMath_NewMat4Ones(void);

/**
 *
 * @return 4x4 matrix with all values to equal 0
 */
beeMat4 *beeMath_NewMat4Zeroes(void);

/**
 *
 * @param x - x value of translation
 * @param y - y value of translation
 * @param z - z value of translation
 * @return 4x4 matrix of translation
 */
beeMat4 *beeMath_NewMat4Translation(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z);

/**
 *
 * @param x - x value of quaternion
 * @param y - y value of quaternion
 * @param z - z value of quaternion
 * @param w - w value of quaternion
 * @return 4x4 matrix of rotation
 */
beeMat4 *beeMath_NewMat4Rotation(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z, BEE_FLOAT w);

/**
 *
 * @param x - x value of scale
 * @param y - y value of scale
 * @param z - z value of scale
 * @return 4x4 matrix of scale
 */
beeMat4 *beeMath_NewMat4Scale(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z);

/**
 *
 * @param fov - horizontal fov angle in degrees
 * @param near - distance to near clipping plane
 * @param far - distance to far clipping plane
 * @return 4x4 matrix of scale
 */
beeMat4 *beeMath_NewMat4Projection(BEE_FLOAT fov, BEE_FLOAT near, BEE_FLOAT far);

/**
 *
 * @return 4x4 matrix with given values
 */
beeMat4 *beeMath_NewMat4Values(
	BEE_FLOAT m00, BEE_FLOAT m01, BEE_FLOAT m02, BEE_FLOAT m03,
	BEE_FLOAT m10, BEE_FLOAT m11, BEE_FLOAT m12, BEE_FLOAT m13,
	BEE_FLOAT m20, BEE_FLOAT m21, BEE_FLOAT m22, BEE_FLOAT m23,
	BEE_FLOAT m30, BEE_FLOAT m31, BEE_FLOAT m32, BEE_FLOAT m33
);

void beeMath_FreeMat4(beeMat4 *mat);

/**
 * Sets all values from <b>other</b> to <b>target</b> matrix
 * @param target - target matrix to copy values to
 * @param source - other matrix to copy values from
 */
void beeMath_Mat4Copy(beeMat4 *target, beeMat4 *source);

/**
 * @param mat - matrix object
 * @param row - row
 * @param col - column
 * @return returns pointer to corresponding matrix element.
 * If either row or column or both out of range - returns <b>NULL</b>
 */
BEE_FLOAT *beeMath_Mat4Get(beeMat4 *mat, int row, int col);

/**
 * Sets corresponding value to matrix element in <b>row</b>, <b>column</b>
 * @param mat - matrix object
 * @param row - row
 * @param col - column
 * @param value - new value
 * @note if either row or column or both out of range - <b>noop</b>
 */
void beeMath_Mat4Set(beeMat4 *mat, int row, int col, BEE_FLOAT value);

/**
 * Checks two matrices objects for equality
 * @param mat1 - matrix 1
 * @param mat2 - matrix 2
 * @return equality result
 */
bool beeMath_Mat4Equal(beeMat4 *mat1, beeMat4 *mat2);

/**
 *
 * @param mat1 - matrix 1, lhs
 * @param mat2 - matrix 1, rhs
 * @return product of 2 matrices. Don't forget to free that matrix after use
 */
beeMat4 *beeMath_Mat4MulMat4(beeMat4 *mat1, beeMat4 *mat2);

/**
 *
 * @param mat - matrix of which inverse will be calculated
 * @param dest - destination matrix which will be written if inverse is possible
 * @return true if inverse matrix exists, false otherwise
 */
bool beeMath_Mat4Inverse(beeMat4 *mat, beeMat4 **dest);

#endif  //BEE_MATRIX4_H