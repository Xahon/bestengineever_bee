#ifndef BEE_ID_H
#define BEE_ID_H

/**
 * generates new unique id
 * @return new id
 */
unsigned long beeEngine_GetNextId(void);

#endif //BEE_ID_H
