#ifndef BEE_BEETYPES_H
#define BEE_BEETYPES_H

#ifndef BEE_FLOAT
#define BEE_FLOAT double
#endif

#ifndef BEE_FLOAT_PREC
#define BEE_FLOAT_PREC (BEE_FLOAT)10e-5
#endif

#endif  // BEE_BEETYPES_H
