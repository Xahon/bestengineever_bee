cmake_minimum_required(VERSION 3.15)
project(BestEngineEver C)

set(CMAKE_C_STANDARD 11)

include(cmake/check.cmake)

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(GLEW REQUIRED)
find_package(Lua REQUIRED)

############ LIB

add_library(
        bee_lib SHARED
        include/bee/beetypes.h
        include/bee/mathf.h
        source/mathf.c
        include/bee/matrix4.h
        source/matrix4.c
        include/bee/vector4.h
        source/vector4.c
        include/bee/engine.h
        source/engine.c
        include/bee/scene.h
        source/scene.c
        include/bee/spatial.h
        source/spatial.c
        include/bee/id.h
        source/id.c
        include/bee/camera.h
        source/camera.c
        include/bee/scripting.h source/scripting.c)

target_include_directories(
        bee_lib
        PUBLIC
        include/
        ${OPENGL_INCLUDE_DIRS}
        ${GLUT_INCLUDE_DIRS}
        ${GLEW_INCLUDE_DIRS}
        ${LUA_INCLUDE_DIR}
)

target_link_libraries(
        bee_lib
        ${OPENGL_LIBRARIES}
        GLUT::GLUT
        GLEW::GLEW
        ${LUA_LIBRARIES}
)

set_target_properties(
        bee_lib PROPERTIES
        LINKER_LANGUAGE C
        LIBRARY_OUTPUT_NAME bee
)

############ EXE

add_executable(
        bee_exe
        source/main.c
)

target_link_libraries(
        bee_exe
        bee_lib
)

set_target_properties(
        bee_exe PROPERTIES
        RUNTIME_OUTPUT_NAME bee
)

############ TEST

enable_testing()
add_executable(
        bee_test
        tests/main.c
)

target_include_directories(
        bee_test
        PRIVATE 3rdparty/check/src/
)

target_link_libraries(
        bee_test
        bee_lib
        ${check_Target}
)
set_target_properties(
        bee_test PROPERTIES
        LIBRARY_OUTPUT_NAME test
)