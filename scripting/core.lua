---Creates a scene
---@param name string name of the scene
---@return number id of object
function CreateScene(name) end

---Creates a camera
---@param near number near clipping plane
---@param far number far clipping plane
---@param fov number horizontal fov angle
---@param aspect number aspect ratio of the camera view
---@return number id of object
function CreateCamera(near, far, fov, aspect) end