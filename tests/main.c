#include <stdlib.h>
#include "check.h"
#include "bee/mathf.h"

START_TEST(matrix_tests) {
	beeMat4 *mat1;
	beeMat4 *mat2;

	mat1 = beeMath_NewMat4Ones();
	mat2 = beeMath_NewMat4Ones();

	ck_assert(beeMath_Mat4Equal(mat1, mat2));

	beeMath_Mat4Set(mat2, 0, 0, 20);

	ck_assert(!beeMath_Mat4Equal(mat1, mat2));

}
END_TEST

Suite *suite(void) {
	Suite *s;
	TCase *tc_core;

	s = suite_create("Matrices");
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, matrix_tests);
	suite_add_tcase(s, tc_core);

	return s;
}

int main(void) {
	int no_failed = 0;
	Suite *s;
	SRunner *runner;

	s = suite();
	runner = srunner_create(s);

	srunner_run_all(runner, CK_NORMAL);
	no_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}