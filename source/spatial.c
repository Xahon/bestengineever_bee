#include <stdlib.h>
#include <math.h>
#include "bee/spatial.h"

beeSpatial *beeSpatial_NewSpatial(void) {
	beeSpatial *spatial = NULL;
	spatial = (beeSpatial *)malloc(sizeof(beeSpatial));
	return spatial;
}

void beeSpatial_FreeSpatial(beeSpatial *spatial) {
	if (spatial != NULL) {
		free(spatial);
	}
}

void beeSpatial_SetPosition(beeSpatial *spatial, BEE_FLOAT px, BEE_FLOAT py, BEE_FLOAT pz) {
	spatial->position.x = px;
	spatial->position.y = py;
	spatial->position.z = pz;
}

void beeSpatial_SetPositionRelative(beeSpatial *spatial, BEE_FLOAT px, BEE_FLOAT py, BEE_FLOAT pz) {
	spatial->position.x += px;
	spatial->position.y += py;
	spatial->position.z += pz;
}

void beeSpatial_SetScale(beeSpatial *spatial, BEE_FLOAT sx, BEE_FLOAT sy, BEE_FLOAT sz) {
	spatial->scale.x = sx;
	spatial->scale.y = sy;
	spatial->scale.z = sz;
}

void beeSpatial_SetRotationEuler(beeSpatial *spatial, BEE_FLOAT rx, BEE_FLOAT ry, BEE_FLOAT rz) {

	// Taken from: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Source_Code

	BEE_FLOAT cy = (BEE_FLOAT)cos(rz * 0.5);
	BEE_FLOAT sy = (BEE_FLOAT)sin(rz * 0.5);
	BEE_FLOAT cp = (BEE_FLOAT)cos(ry * 0.5);
	BEE_FLOAT sp = (BEE_FLOAT)sin(ry * 0.5);
	BEE_FLOAT cr = (BEE_FLOAT)cos(rx * 0.5);
	BEE_FLOAT sr = (BEE_FLOAT)sin(rx * 0.5);

	spatial->rotation.x = cy * cp * sr - sy * sp * cr;
	spatial->rotation.y = sy * cp * sr + cy * sp * cr;
	spatial->rotation.z = sy * cp * cr - cy * sp * sr;
	spatial->rotation.w = cy * cp * cr + sy * sp * sr;
}

beeMat4 *beeSpatial_GetModelMatrix(beeSpatial *spatial) {
	beeMat4 *kMat;
	beeMat4 *kTranslation;
	beeMat4 *kRotation;
	beeMat4 *kScale;

	kMat = beeMath_NewMat4Ones();
	kTranslation = beeMath_NewMat4Translation(spatial->position.x, spatial->position.y, spatial->position.z);
	kRotation = beeMath_NewMat4Rotation(spatial->rotation.x, spatial->rotation.y,
										spatial->rotation.z, spatial->rotation.w);
	kScale = beeMath_NewMat4Scale(spatial->scale.x, spatial->scale.y, spatial->scale.z);

	beeMat4 *kTempMatrix;

	kTempMatrix = beeMath_Mat4MulMat4(kMat, kTranslation);
	beeMath_Mat4Copy(kMat, kTempMatrix);
	beeMath_FreeMat4(kTempMatrix);

	kTempMatrix = beeMath_Mat4MulMat4(kMat, kRotation);
	beeMath_Mat4Copy(kMat, kTempMatrix);
	beeMath_FreeMat4(kTempMatrix);

	kTempMatrix = beeMath_Mat4MulMat4(kMat, kScale);
	beeMath_Mat4Copy(kMat, kTempMatrix);
	beeMath_FreeMat4(kTempMatrix);

	beeMath_FreeMat4(kTranslation);
	beeMath_FreeMat4(kRotation);
	beeMath_FreeMat4(kScale);

	return kMat;
}
