#include "bee/camera.h"
#include "bee/engine.h"
#include "bee/scene.h"

int main(int argc, char **argv) {
	beeScene *gameScene = beeObjects_NewScene("Game Scene");
	beeCamera *gameCamera = beeObjects_NewCamera(0.01, 300.0, 90, 800.0 / 600);

	beeObjects_SetSceneCamera(gameScene, gameCamera);
	beeEngine_RegisterScene(gameScene);

	int resultCode = beeEngine_Initialize(argc, argv);
	beeObjects_FreeScene(gameScene);
	beeObjects_FreeCamera(gameCamera);
	beeObjects_FreeScene(gameScene);
	beeEngine_Deinitialize();

	return resultCode;
}