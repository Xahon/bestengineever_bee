#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "bee/mathf.h"
#include "bee/matrix4.h"

beeMat4 *beeMath_NewMat4Ones(void) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Values(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Zeroes(void) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Values(
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Translation(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Values(
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Rotation(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z, BEE_FLOAT w) {
	beeMat4 *kMat = NULL;

	// taken from: https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm

	BEE_FLOAT m00, m01, m02, m10, m11, m12, m20, m21, m22;

	double
		sqw = w * w,
		sqx = x * x,
		sqy = y * y,
		sqz = z * z,
		invs = 1 / (sqx + sqy + sqz + sqw);

	m00 = (BEE_FLOAT)((sqx - sqy - sqz + sqw) * invs);
	m11 = (BEE_FLOAT)((-sqx + sqy - sqz + sqw) * invs);
	m22 = (BEE_FLOAT)((-sqx - sqy + sqz + sqw) * invs);

	double
		tmp1 = x * y,
		tmp2 = z * w;

	m10 = (BEE_FLOAT)(2.0 * (tmp1 + tmp2) * invs);
	m01 = (BEE_FLOAT)(2.0 * (tmp1 - tmp2) * invs);

	tmp1 = x * z;
	tmp2 = y * w;

	m20 = (BEE_FLOAT)(2.0 * (tmp1 - tmp2) * invs);
	m02 = (BEE_FLOAT)(2.0 * (tmp1 + tmp2) * invs);

	tmp1 = y * z;
	tmp2 = x * w;

	m21 = (BEE_FLOAT)(2.0 * (tmp1 + tmp2) * invs);
	m12 = (BEE_FLOAT)(2.0 * (tmp1 - tmp2) * invs);

	kMat = beeMath_NewMat4Values(
		m00, m01, m02, 0,
		m10, m11, m12, 0,
		m20, m21, m22, 0,
		0, 0, 0, 1
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Scale(BEE_FLOAT x, BEE_FLOAT y, BEE_FLOAT z) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Values(
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Projection(BEE_FLOAT fov, BEE_FLOAT near, BEE_FLOAT far) {
	beeMat4 *kMat = NULL;
	BEE_FLOAT s = (BEE_FLOAT)(1.0 / (tan(fov / 2.0 * beeMath_Deg2Rad)));
	BEE_FLOAT fmn = (BEE_FLOAT)(far - near);
	BEE_FLOAT m22 = -((BEE_FLOAT)far / fmn);
	BEE_FLOAT m32 = -(far * near / fmn);

	kMat = beeMath_NewMat4Values(
		s, 0, 0, 0,
		0, s, 0, 0,
		0, 0, m22, -1,
		0, 0, m32, 0
	);
	return kMat;
}

beeMat4 *beeMath_NewMat4Values(
	BEE_FLOAT m00, BEE_FLOAT m01, BEE_FLOAT m02, BEE_FLOAT m03,
	BEE_FLOAT m10, BEE_FLOAT m11, BEE_FLOAT m12, BEE_FLOAT m13,
	BEE_FLOAT m20, BEE_FLOAT m21, BEE_FLOAT m22, BEE_FLOAT m23,
	BEE_FLOAT m30, BEE_FLOAT m31, BEE_FLOAT m32, BEE_FLOAT m33
) {
	beeMat4 *kMat = NULL;
	kMat = (beeMat4 *)malloc(sizeof(beeMat4));
	memset(&kMat->m, 0, 16);
	kMat->m[0] = (BEE_FLOAT)1;
	kMat->m[5] = (BEE_FLOAT)1;
	kMat->m[10] = (BEE_FLOAT)1;
	kMat->m[15] = (BEE_FLOAT)1;
	return kMat;
}

void beeMath_FreeMat4(beeMat4 *mat) {
	if (mat != NULL) {
		free(mat);
	}
}

void beeMath_Mat4Copy(beeMat4 *target, beeMat4 *source) {
	memcpy(target->m, source->m, sizeof(BEE_FLOAT) * 16);
}

BEE_FLOAT *beeMath_Mat4Get(beeMat4 *mat, int row, int col) {
	if (row < 0 || row >= 4 || col < 0 || col >= 4) {
		return NULL;
	}
	return &mat->m[row * 4 + col];
}

void beeMath_Mat4Set(beeMat4 *mat, int row, int col, BEE_FLOAT value) {
	if (row < 0 || row >= 4 || col < 0 || col >= 4) {
		return;
	}
	*beeMath_Mat4Get(mat, row, col) = value;
}

bool beeMath_Mat4Equal(beeMat4 *mat1, beeMat4 *mat2) {
	for (int i = 0; i < 16; ++i) {
		if (!beeMath_FloatEqPrecDef(mat1->m[i], mat2->m[i])) {
			return false;
		}
	}
	return true;
}

beeMat4 *beeMath_Mat4MulMat4(beeMat4 *mat1, beeMat4 *mat2) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Zeroes();

	for (int r = 0; r < 4; ++r) {
		for (int c = 0; c < 4; ++c) {
			BEE_FLOAT value = 0;
			for (int i = 0; i < 4; ++i) {
				value += *beeMath_Mat4Get(mat1, r, i) * *beeMath_Mat4Get(mat2, i, c);
			}
			beeMath_Mat4Set(kMat, r, c, value);
		}
	}

	return kMat;
}

bool beeMath_Mat4Inverse(beeMat4 *mat, beeMat4 **dest) {
	beeMat4 *kMat = NULL;
	if (*dest == NULL) {
		kMat = beeMath_NewMat4Zeroes();
	} else {
		kMat = *dest;
	}

	kMat->m[0] = mat->m[5] * mat->m[10] * mat->m[15] -
		mat->m[5] * mat->m[11] * mat->m[14] -
		mat->m[9] * mat->m[6] * mat->m[15] +
		mat->m[9] * mat->m[7] * mat->m[14] +
		mat->m[13] * mat->m[6] * mat->m[11] -
		mat->m[13] * mat->m[7] * mat->m[10];

	kMat->m[4] = -mat->m[4] * mat->m[10] * mat->m[15] +
		mat->m[4] * mat->m[11] * mat->m[14] +
		mat->m[8] * mat->m[6] * mat->m[15] -
		mat->m[8] * mat->m[7] * mat->m[14] -
		mat->m[12] * mat->m[6] * mat->m[11] +
		mat->m[12] * mat->m[7] * mat->m[10];

	kMat->m[8] = mat->m[4] * mat->m[9] * mat->m[15] -
		mat->m[4] * mat->m[11] * mat->m[13] -
		mat->m[8] * mat->m[5] * mat->m[15] +
		mat->m[8] * mat->m[7] * mat->m[13] +
		mat->m[12] * mat->m[5] * mat->m[11] -
		mat->m[12] * mat->m[7] * mat->m[9];

	kMat->m[12] = -mat->m[4] * mat->m[9] * mat->m[14] +
		mat->m[4] * mat->m[10] * mat->m[13] +
		mat->m[8] * mat->m[5] * mat->m[14] -
		mat->m[8] * mat->m[6] * mat->m[13] -
		mat->m[12] * mat->m[5] * mat->m[10] +
		mat->m[12] * mat->m[6] * mat->m[9];

	kMat->m[1] = -mat->m[1] * mat->m[10] * mat->m[15] +
		mat->m[1] * mat->m[11] * mat->m[14] +
		mat->m[9] * mat->m[2] * mat->m[15] -
		mat->m[9] * mat->m[3] * mat->m[14] -
		mat->m[13] * mat->m[2] * mat->m[11] +
		mat->m[13] * mat->m[3] * mat->m[10];

	kMat->m[5] = mat->m[0] * mat->m[10] * mat->m[15] -
		mat->m[0] * mat->m[11] * mat->m[14] -
		mat->m[8] * mat->m[2] * mat->m[15] +
		mat->m[8] * mat->m[3] * mat->m[14] +
		mat->m[12] * mat->m[2] * mat->m[11] -
		mat->m[12] * mat->m[3] * mat->m[10];

	kMat->m[9] = -mat->m[0] * mat->m[9] * mat->m[15] +
		mat->m[0] * mat->m[11] * mat->m[13] +
		mat->m[8] * mat->m[1] * mat->m[15] -
		mat->m[8] * mat->m[3] * mat->m[13] -
		mat->m[12] * mat->m[1] * mat->m[11] +
		mat->m[12] * mat->m[3] * mat->m[9];

	kMat->m[13] = mat->m[0] * mat->m[9] * mat->m[14] -
		mat->m[0] * mat->m[10] * mat->m[13] -
		mat->m[8] * mat->m[1] * mat->m[14] +
		mat->m[8] * mat->m[2] * mat->m[13] +
		mat->m[12] * mat->m[1] * mat->m[10] -
		mat->m[12] * mat->m[2] * mat->m[9];

	kMat->m[2] = mat->m[1] * mat->m[6] * mat->m[15] -
		mat->m[1] * mat->m[7] * mat->m[14] -
		mat->m[5] * mat->m[2] * mat->m[15] +
		mat->m[5] * mat->m[3] * mat->m[14] +
		mat->m[13] * mat->m[2] * mat->m[7] -
		mat->m[13] * mat->m[3] * mat->m[6];

	kMat->m[6] = -mat->m[0] * mat->m[6] * mat->m[15] +
		mat->m[0] * mat->m[7] * mat->m[14] +
		mat->m[4] * mat->m[2] * mat->m[15] -
		mat->m[4] * mat->m[3] * mat->m[14] -
		mat->m[12] * mat->m[2] * mat->m[7] +
		mat->m[12] * mat->m[3] * mat->m[6];

	kMat->m[10] = mat->m[0] * mat->m[5] * mat->m[15] -
		mat->m[0] * mat->m[7] * mat->m[13] -
		mat->m[4] * mat->m[1] * mat->m[15] +
		mat->m[4] * mat->m[3] * mat->m[13] +
		mat->m[12] * mat->m[1] * mat->m[7] -
		mat->m[12] * mat->m[3] * mat->m[5];

	kMat->m[14] = -mat->m[0] * mat->m[5] * mat->m[14] +
		mat->m[0] * mat->m[6] * mat->m[13] +
		mat->m[4] * mat->m[1] * mat->m[14] -
		mat->m[4] * mat->m[2] * mat->m[13] -
		mat->m[12] * mat->m[1] * mat->m[6] +
		mat->m[12] * mat->m[2] * mat->m[5];

	kMat->m[3] = -mat->m[1] * mat->m[6] * mat->m[11] +
		mat->m[1] * mat->m[7] * mat->m[10] +
		mat->m[5] * mat->m[2] * mat->m[11] -
		mat->m[5] * mat->m[3] * mat->m[10] -
		mat->m[9] * mat->m[2] * mat->m[7] +
		mat->m[9] * mat->m[3] * mat->m[6];

	kMat->m[7] = mat->m[0] * mat->m[6] * mat->m[11] -
		mat->m[0] * mat->m[7] * mat->m[10] -
		mat->m[4] * mat->m[2] * mat->m[11] +
		mat->m[4] * mat->m[3] * mat->m[10] +
		mat->m[8] * mat->m[2] * mat->m[7] -
		mat->m[8] * mat->m[3] * mat->m[6];

	kMat->m[11] = -mat->m[0] * mat->m[5] * mat->m[11] +
		mat->m[0] * mat->m[7] * mat->m[9] +
		mat->m[4] * mat->m[1] * mat->m[11] -
		mat->m[4] * mat->m[3] * mat->m[9] -
		mat->m[8] * mat->m[1] * mat->m[7] +
		mat->m[8] * mat->m[3] * mat->m[5];

	kMat->m[15] = mat->m[0] * mat->m[5] * mat->m[10] -
		mat->m[0] * mat->m[6] * mat->m[9] -
		mat->m[4] * mat->m[1] * mat->m[10] +
		mat->m[4] * mat->m[2] * mat->m[9] +
		mat->m[8] * mat->m[1] * mat->m[6] -
		mat->m[8] * mat->m[2] * mat->m[5];

	BEE_FLOAT det = mat->m[0] * kMat->m[0] + mat->m[1] * kMat->m[4] + mat->m[2] * kMat->m[8] + mat->m[3] * kMat->m[12];

	if (det == 0)
		return false;

	det = 1.0 / det;

	for (int i = 0; i < 16; i++)
		kMat->m[i] = kMat->m[i] * det;

	*dest = kMat;

	return true;
}
