#include <stdlib.h>
#include "bee/id.h"
#include "bee/camera.h"

beeCamera *beeObjects_NewCamera(BEE_FLOAT near, BEE_FLOAT far, BEE_FLOAT fov, BEE_FLOAT aspect) {
	beeCamera *kCamera = NULL;
	kCamera = (beeCamera *)malloc(sizeof(beeCamera));
	kCamera->id = beeEngine_GetNextId();
	kCamera->spatial = beeSpatial_NewSpatial();
	kCamera->near = near;
	kCamera->far = far;
	kCamera->fov = fov;
	kCamera->aspect = aspect;
	return kCamera;
}

void beeObjects_FreeCamera(beeCamera *camera) {
	if (camera != NULL) {
		beeSpatial_FreeSpatial(camera->spatial);
		free(camera);
	}
}

beeMat4 *beeObjects_GetProjectionMatrix(beeCamera *camera) {
	beeMat4 *kMat = NULL;
	kMat = beeMath_NewMat4Projection(camera->fov, camera->near, camera->far);
	return kMat;
}


/// SCRIPTING ///

int beeScriptingBinding_CreateCamera(lua_State *L) {
	beeCamera *camera = NULL;
	double kNear = lua_tonumber(L, 1);
	double kFar = lua_tonumber(L, 2);
	double kFov = lua_tonumber(L, 3);
	double kAspect = lua_tonumber(L, 4);

	camera = beeObjects_NewCamera(kNear, kFar, kFov, kAspect);

	lua_pushinteger(L, camera->id);
	return 1;
}
