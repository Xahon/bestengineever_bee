#include <string.h>
#include "bee/scripting.h"

lua_State *mLuaState;

void beeScripting_InitScripting(void);
void beeScripting_DeinitScripting(void);
void beeScripting_CallLUAFunction(const char *name, int inputs, int outputs);
void beeScripting_RegisterNativeFuncs(void);

void beeScripting_InitScripting(void) {
	mLuaState = luaL_newstate();
	luaL_openlibs(mLuaState);

	luaL_dofile(mLuaState, "init.lua");
	beeScripting_RegisterNativeFuncs();
}

void beeScripting_DeinitScripting(void) {
	lua_close(mLuaState);
}

void beeScripting_CallLUAFunction(const char *name, int inputs, int outputs) {
	lua_getglobal(mLuaState, name);
	if (lua_pcall(mLuaState, inputs, outputs, 0) != 0) {
		char err[512];
		strcat(err, "'");
		strcat(err, name);
		strcat(err, "' function not found in script");
		perror(err);
	}
}

#include "bee/scene.h"
#include "bee/camera.h"

void beeScripting_RegisterNativeFuncs(void) {
	lua_register(mLuaState, "CreateScene", beeScriptingBinding_CreateScene);
	lua_register(mLuaState, "CreateCamera", beeScriptingBinding_CreateCamera);
}