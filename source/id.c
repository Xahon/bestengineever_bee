#include "bee/id.h"

static unsigned long mId;

unsigned long beeEngine_GetNextId(void) {
	return ++mId;
}
