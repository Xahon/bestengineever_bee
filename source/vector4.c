#include <stdlib.h>
#include <math.h>
#include "bee/mathf.h"
#include "bee/vector4.h"

beeVec4 *beeMath_NewVec4(void) {
	beeVec4 *kVec = NULL;
	kVec = (beeVec4 *)malloc(sizeof(beeVec4));
	kVec->x = 0;
	kVec->y = 0;
	kVec->z = 0;
	kVec->w = 0;
	return kVec;
}

void beeMath_FreeVec4(beeVec4 *vector) {
	if (vector != NULL) {
		free(vector);
	}
}

void beeMath_Vec4Copy(beeVec4 *target, beeVec4 *source) {
	target->x = source->x;
	target->y = source->y;
	target->z = source->z;
	target->w = source->w;
}

BEE_FLOAT beeMath_Vec4MagnitudeSqr(beeVec4 *vec) {
	return vec->x * vec->x + vec->y * vec->y + vec->z * vec->z + vec->w * vec->w;
}

BEE_FLOAT beeMath_Vec4Magnitude(beeVec4 *vec) {
	BEE_FLOAT kMagSqr = beeMath_Vec4MagnitudeSqr(vec);
	if (beeMath_FloatEqPrecDef(kMagSqr, 1.0)) {
		return kMagSqr;
	}
	return (BEE_FLOAT)sqrt(kMagSqr);
}

void beeMath_Vec4Normalize(beeVec4 *vec) {
	BEE_FLOAT kMag = beeMath_Vec4Magnitude(vec);
	if (beeMath_FloatEqPrecDef(kMag, 1.0)) {
		return;
	}
	if (beeMath_FloatEqPrecDef(kMag, 0.0)) {
		return;
	}
	vec->x /= kMag;
	vec->y /= kMag;
	vec->z /= kMag;
	vec->w /= kMag;
}

beeVec4 *beeMath_Vec4Normalized(beeVec4 *vec) {
	beeVec4 *kVec = NULL;
	kVec = beeMath_NewVec4();
	beeMath_Vec4Copy(kVec, vec);
	beeMath_Vec4Normalize(kVec);
	return kVec;
}

BEE_FLOAT beeMath_Vec4DotVec4(beeVec4 *vec1, beeVec4 *vec2) {
	return vec1->x * vec2->x + vec1->y * vec2->y + vec1->z * vec2->z + vec1->w * vec2->w;
}

beeVec4 *beeMath_Vec4CrossVec4(beeVec4 *vec1, beeVec4 *vec2) {
	beeVec4 *kVec = NULL;
	kVec = beeMath_NewVec4();

	kVec->x = vec1->y * vec2->z - vec1->z * vec2->y;
	kVec->y = vec1->x * vec2->z - vec1->z * vec2->x;
	kVec->z = vec1->x * vec2->y - vec1->y * vec2->x;
	kVec->w = 0;

	return kVec;
}


