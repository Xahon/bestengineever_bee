#include <math.h>
#include "bee/mathf.h"

const BEE_FLOAT beeMath_PI = (BEE_FLOAT)3.14159265358979323846;
const BEE_FLOAT beeMath_Rad2Deg = (BEE_FLOAT)(180.0 / beeMath_PI);
const BEE_FLOAT beeMath_Deg2Rad = (BEE_FLOAT)(beeMath_PI / 180.0);

bool beeMath_FloatEqPrec(BEE_FLOAT f1, BEE_FLOAT f2, BEE_FLOAT prec) {
	return (BEE_FLOAT)fabs(f1 - f2) <= prec;
}

bool beeMath_FloatEqPrecDef(BEE_FLOAT f1, BEE_FLOAT f2) {
	return (BEE_FLOAT)fabs(f1 - f2) <= BEE_FLOAT_PREC;
}

