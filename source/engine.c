#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "bee/mathf.h"
#include "bee/engine.h"
#include "bee/scripting.h"

#define BEE_GL_TIMER__RENDER 0

const char *mTitle = "BEE";

int mCurrentWidth = 800;
int mCurrentHeight = 600;
int mWindowHandle = 0;
int mFrameCount = 0;
int mTargetFPS = 60;

clock_t mTimeStarted = 0;
clock_t mTimeCurrFrameUs = 0;
clock_t mTimePrevFrameUs = 0;
clock_t mDeltaTimeUs = 0;
int mClockWrapArounds = 0;

int mRegisteredScenesPoolCount = 0;
int mRegisteredScenesCount = 0;
const int mRegisteredScenesGrow = 4;
beeScene **mRegisteredScenes = NULL;

void beeEngine_InitLUA();
void beeEngine_InitWindow(int, char **);
void beeEngine_ResizeFunction(int, int);
void beeEngine_RenderFunction(int);
void beeEngine_IdleFunction(void);
void beeEngine_RecalculateTime(void);
void beeEngine_OnStart(void);
void beeEngine_OnEarlyUpdate(void);
void beeEngine_OnLateUpdate(void);

int beeEngine_Initialize(int argc, char **glutArgs) {
	beeEngine_InitWindow(argc, glutArgs);

	fprintf(stdout, "INFO: OpenGL Version: %s\n", glGetString(GL_VERSION));

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glutMainLoop();

	return EXIT_SUCCESS;
}

void beeEngine_Deinitialize(void) {
	beeScripting_DeinitScripting();
}

void beeEngine_InitWindow(int argc, char *argv[]) {
	glutInit(&argc, argv);

	glutInitContextVersion(4, 0);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutInitWindowSize(mCurrentWidth, mCurrentHeight);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	mWindowHandle = glutCreateWindow(mTitle);

	if (mWindowHandle < 1) {
		fprintf(stderr, "ERROR: Could not create a new rendering window.\n");
		exit(EXIT_FAILURE);
	}

	glutReshapeFunc(beeEngine_ResizeFunction);
	glutIdleFunc(beeEngine_IdleFunction);

	beeEngine_OnStart();

	glutTimerFunc(0, beeEngine_RenderFunction, BEE_GL_TIMER__RENDER);
}

void beeEngine_ResizeFunction(int Width, int Height) {
	mCurrentWidth = Width;
	mCurrentHeight = Height;
	glViewport(0, 0, mCurrentWidth, mCurrentHeight);
}

void beeEngine_RenderFunction(int value) {
	beeEngine_OnEarlyUpdate();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glutSwapBuffers();
	glutPostRedisplay();
	beeEngine_OnLateUpdate();

	glutTimerFunc(1000 / mTargetFPS, beeEngine_RenderFunction, BEE_GL_TIMER__RENDER);
}

void beeEngine_InitLUA() {
	beeScripting_InitScripting();
	beeScripting_CallLUAFunction("init", 0, 0);
}

void beeEngine_IdleFunction(void) {
	glutPostRedisplay();
}

void beeEngine_OnStart(void) {
	mFrameCount = 0;
	mTimeStarted = mTimeCurrFrameUs = mTimePrevFrameUs = clock();
	beeEngine_InitLUA();
}

void beeEngine_OnEarlyUpdate(void) {
	++mFrameCount;
}

void beeEngine_OnLateUpdate(void) {
	beeEngine_RecalculateTime();
}
void beeEngine_RecalculateTime(void) {
	mTimeCurrFrameUs = clock();
	if (mTimeCurrFrameUs > mTimePrevFrameUs) {
		mDeltaTimeUs = mTimeCurrFrameUs - mTimePrevFrameUs;
	} else {
		++mClockWrapArounds;
		mDeltaTimeUs = LONG_MAX - mTimePrevFrameUs + mTimeCurrFrameUs;
	}
	mTimePrevFrameUs = mTimeCurrFrameUs;
}

float beeEngine_GetFPS(void) {
	float dt = beeEngine_GetDeltaTime();
	if (beeMath_FloatEqPrecDef(dt, 0)) {
		return 0.0f;
	}
	return 1.0f / dt;
}

float beeEngine_GetDeltaTime(void) {
	return (float)mDeltaTimeUs / CLOCKS_PER_SEC;
}

void beeEngine_ExtendRegisteredScenesArray(void) {
	int kNewSize = mRegisteredScenesPoolCount + mRegisteredScenesGrow;
	beeScene **kRealloced = (beeScene **)realloc(mRegisteredScenes, sizeof(void *) * kNewSize);
	if (kRealloced) {
		free(mRegisteredScenes);
		mRegisteredScenes = kRealloced;
	}
	mRegisteredScenesPoolCount = kNewSize;
}

int beeEngine_RegisterScene(beeScene *scene) {
	if (mRegisteredScenesPoolCount <= 0 || mRegisteredScenesCount >= mRegisteredScenesPoolCount) {
		beeEngine_ExtendRegisteredScenesArray();
	}

	int kFreeIndex = -1;

	for (int i = 0; i < mRegisteredScenesPoolCount; ++i) {
		beeScene *kScene = NULL;
		kScene = mRegisteredScenes[i];
		if (kScene == NULL) {
			if (kFreeIndex == -1) {
				kFreeIndex = i;
			}
			continue;
		}

		if (scene->id == kScene->id) {
			return -1;
		}
	}

	if (kFreeIndex == -1) {
		beeEngine_ExtendRegisteredScenesArray();
		return beeEngine_RegisterScene(scene);
	}

	++mRegisteredScenesCount;
	mRegisteredScenes[kFreeIndex] = scene;

	return 0;
}

void beeEngine_UnregisterScene(beeScene *scene) {
	for (int i = 0; i < mRegisteredScenesCount; ++i) {
		beeScene *kScene = mRegisteredScenes[i];
		if (kScene == NULL) {
			continue;
		}
		if (scene->id == kScene->id) {
			mRegisteredScenes[i] = NULL;
			return;
		}
	}
}

