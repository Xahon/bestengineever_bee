#include <stdlib.h>
#include "bee/engine.h"
#include "bee/id.h"
#include "bee/scene.h"

beeScene *beeObjects_NewScene(const char *name) {
	beeScene *scene = NULL;
	scene = (beeScene *)malloc(sizeof(beeScene));
	scene->id = beeEngine_GetNextId();
	scene->name = name;
	scene->spatial = beeSpatial_NewSpatial();

	return scene;
}

void beeObjects_FreeScene(beeScene *scene) {
	if (scene != NULL) {
		beeSpatial_FreeSpatial(scene->spatial);
		free(scene);
	}
}

void beeObjects_SetSceneCamera(beeScene *scene, beeCamera *camera) {
	scene->camera = camera;
}

void beeObjects_RenderScene(beeScene *scene) {
	if (scene->camera == NULL) {
		return;
	}
}


/// SCRIPTING ///

int beeScriptingBinding_CreateScene(lua_State *L) {
	beeScene *scene = NULL;
	const char *kName = lua_tostring(L, 1);

	scene = beeObjects_NewScene(kName);
	beeEngine_RegisterScene(scene);
	lua_pushinteger(L, scene->id);
	return 1;
}
